#! /bin/sh

find . \
  -type f -name '*.py[co]' -delete \
  -o \
  -type d -name __pycache__ -delete \
  -o \
  -type d -name '.pytest_cache' -exec rm -rf {} \;
