'''
Python3 function to print sum of all the elements of a json binary tree
'''


def b_tree_sum_nodes(node):
    result = node['root']
    if "left" in node:
        result += b_tree_sum_nodes(node["left"])
    if "right" in node:
        result += b_tree_sum_nodes(node["right"])
    return result
