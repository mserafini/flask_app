#!/usr/bin/env python

from middleware.prom_middleware import setup_metrics
from middleware.tree_middleware import b_tree_sum_nodes
from constants import SCHEMA, CONTENT_TYPE_JSON, CONTENT_TYPE_PROM_LATEST
from flask import Flask, Response, request
from jsonschema import validate
from jsonschema.exceptions import ValidationError
import prometheus_client
import json
import os

app = Flask(__name__)
setup_metrics(app)


@app.route('/')
def welcome():
    return Response('''
<p>Sample containerized flask app with prometheus telemetry</p>
<p>it works and systemd correctly updates the container image</p>
<p>also ansible configuration works</p>
<p>and the multi pipeline trigger from terraform in gitlab ci/cd</p>
''')


@app.route('/test/', methods=['POST'])
def test():
    try:
        validate(request.json, SCHEMA)
        j_tree = request.json
    except ValidationError as err:
        resp = str(err)
    except Exception:
        resp = 'ERROR send a valid json binary tree'
    else:
        resp = Response(
            json.dumps(
                {
                    "Sum": b_tree_sum_nodes(j_tree)
                }
            ),
            mimetype=CONTENT_TYPE_JSON
        )
    return resp


@app.route('/metrics/')
def metrics():
    return Response(
            prometheus_client.generate_latest(),
            mimetype=CONTENT_TYPE_PROM_LATEST
        )


@app.errorhandler(500)
def handle_500(error):
    return str(error), 500


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
