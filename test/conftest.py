import pytest
from constants import SCHEMA
from jsonschema import validate
import random


def recursive_jb_tree(i_list):
    node = {}
    if not i_list:
        return node

    node["root"] = i_list[0]
    if len(i_list) == 1:
        return node

    new_list = i_list[1:]
    new_list_len = len(new_list)
    if new_list_len > 2:
        branch = random.choice(["left", "right", "both"])
    else:
        branch = random.choice(["left", "right"])

    if branch == "both":
        split_index = random.choice(range(1, len(new_list)-1))
        node["left"] = recursive_jb_tree(new_list[:split_index])
        node["right"] = recursive_jb_tree(new_list[split_index:])
    else:
        node[branch] = recursive_jb_tree(new_list)

    validate(node, SCHEMA)
    return node


@pytest.fixture
def generate_random_int_list():
    i_list = list()
    for _ in range(25):
        rval = random.randint(0, 100)
        i_list.append(rval)
    return i_list


@pytest.fixture
def generate_random_json_tree(generate_random_int_list):
    return recursive_jb_tree(generate_random_int_list)
