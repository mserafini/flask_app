from middleware.tree_middleware import b_tree_sum_nodes


def test_tree_sum(generate_random_int_list, generate_random_json_tree):
    list_sum = sum(generate_random_int_list)
    tree_sum = b_tree_sum_nodes(generate_random_json_tree)
    assert tree_sum == list_sum
