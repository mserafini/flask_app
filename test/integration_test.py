from app import app


def test_base_app():
    response = app.test_client().get('/')
    res = response.data.decode('utf-8')
    assert response.status_code == 200
    assert type(res) is str


def test_b_tree_sum(generate_random_int_list, generate_random_json_tree):
    list_sum = sum(generate_random_int_list)
    response = app.test_client().post("/test/", json=generate_random_json_tree)
    result = response.json["Sum"]
    assert result == list_sum
    assert response.status_code == 200
