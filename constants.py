
CONTENT_TYPE_PROM_LATEST = str('text/plain; version=0.0.4; charset=utf-8')
CONTENT_TYPE_JSON = str('application/json; version=0.0.4; charset=utf-8')

SCHEMA = {
    "type": "object",
    "required": ["root"],
    "properties": {
        "root": {
            "type": "integer"
        },
        "left": {
            '$ref': '#',
        },
        "right": {
            '$ref': '#',
        }
    },
    "additionalProperties": False
}
