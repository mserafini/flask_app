FROM python:3.10-alpine

WORKDIR /app

COPY . .

RUN pip install -r requirements.txt

RUN chmod u+x app.py

LABEL io.containers.autoupdate=registry

ENTRYPOINT ["/app/app.py"]
